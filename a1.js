//MongoDB Activity


db.users.insertMany([
		{
			"firstName": "Trevor",
			"lastName": "McNevan",
			"email": "trevormcnevan@mail.com",
			"password": "TrevNevan123",
			"isAdmin": false
		},
		{
			"firstName": "Jeremy",
			"lastName": "Smith",
			"email": "jeremysmith@mail.com",
			"password": "JSmith456",
			"isAdmin": false
		},
		{
			"firstName": "Steve",
			"lastName": "Augustine",
			"email": "steveaugustine@mail.com",
			"password": "ASteve789",
			"isAdmin": false
		},
		{
			"firstName": "John",
			"lastName": "Bunner",
			"email": "johnbunner@mail.com",
			"password": "BJohn987",
			"isAdmin": false
		},
		{
			"firstName": "Justin",
			"lastName": "Smith",
			"email": "justinsmith@mail.com",
			"password": "Justin654",
			"isAdmin": false
		}
])

db.courses.insertMany([
		{
			"courseName": "Intro to HTML",
			"price": 3000,
			"isActive": false
		},
		{
			"courseName": "Intro to CSS",
			"price": 6000,
			"isActive": false
		},
		{
			"courseName": "Intro to JavaScript",
			"price": 9000,
			"isActive": false
		}
])

db.users.find(
	{
	"isAdmin": false
	}
)

db.users.updateOne(
	{
		
	},
	{
		$set: {
			"isAdmin": true
		}
	})

db.courses.updateOne(
	{
		"courseName": "Intro to JavaScript"	
	},
	{
		$set: {
			"isActive": true
		}
	})

db.courses.deleteMany(
	{
		"isActive": false
	}
)



//Query setup based on instructions
/*
db.users.insertMany([
		{
			"firstName": "",
			"lastName": "",
			"email": "@mail.com",
			"password": "password123",
			"isAdmin": false
		},
		{
			"firstName": "",
			"lastName": "",
			"email": "@mail.com",
			"password": "password123",
			"isAdmin": false
		},
		{
			"firstName": "",
			"lastName": "",
			"email": "@mail.com",
			"password": "password123",
			"isAdmin": false
		},
		{
			"firstName": "",
			"lastName": "",
			"email": "@mail.com",
			"password": "password123",
			"isAdmin": false
		},
		{
			"firstName": "",
			"lastName": "",
			"email": "@mail.com",
			"password": "password123",
			"isAdmin": false
		}
])

db.courses.insertMany([
		{
			"courseName": "",
			"price": ,
			"isActive": false
		},
		{
			"courseName": "",
			"price": ,
			"isActive": false
		},
		{
			"courseName": "",
			"price": ,
			"isActive": false
		}
])

db.users.find(
	{
	"isAdmin": false
	}
)

db.users.updateOne(
	{
		
	},
	{
		$set: {
			"isAdmin": true
		}
	})

db.courses.updateOne(
	{
		"courseName": ""	
	},
	{
		$set: {
			"isActive": true
		}
	})

db.courses.deleteMany(
	{
		"isActive": false
	}
)
*/